#ifndef CL_HELPER_H
#define CL_HELPER_H

#if __APPLE__
   #include <OpenCL/opencl.h>
#else
   #include <CL/cl.h>
#endif

#include <map>
#include <chrono>
#include <vector>

using namespace std;

/* TODO - cleanup these / redo */
struct RegionPtr 
{
    cl_mem      gpuPtr;
    void*       cpuPtr;
    size_t      size;
    bool        isUMA;
    bool        isCpuMap;
};

struct ProfileEvent
{
    long long start;
    long long stop;
};

class Profiler
{
    // start of time, early in application
    chrono::steady_clock::time_point startOfTime;

    // store start-end sets
    map<string, ProfileEvent> tmpListExecTime;
    map<string, vector<ProfileEvent> > listExecTime;

public:
    Profiler();
    void start(string name);
    void stop(string name);
    void printStats();

};

// unique global profiler
extern Profiler GlobalProfiler;

class RamRegion
{
    map<string, RegionPtr> listRegions; 

public:
    RegionPtr* getByName(string name); 
    void alloc(string name, size_t size, bool isUMA); 
    void free(string name); 
    bool isUMA(string name);    
    void printFloat(string name, int elemOffset, int elemNum);

    void* getCpuPtr(string name); 
    void* getGpuPtr(string name); 
    void* getCpuPtr(string name, size_t bufOffset, size_t bufSize); 
    void* getGpuPtr(string name, size_t bufOffset, size_t bufSize); 
};

void gpuGlobalInit(uint platform_select, uint device_select);

int CL_ERR(int cl_ret);
int CL_COMPILE_ERR(int cl_ret, cl_program program, cl_device_id device);

const char* cl_get_string_err(cl_int err);
void cl_get_compiler_err_log(cl_program program, cl_device_id device);

void read_args(int argc, char** argv);
void read_kernel(string file_name, string &str_kernel);

#define DIE(assertion, call_description)                    \
do {                                                        \
    if (assertion) {                                        \
            fprintf(stderr, "(%d): ",                       \
                            __LINE__);                      \
            perror(call_description);                       \
            exit(EXIT_FAILURE);                             \
    }                                                       \
} while(0);

/**
 * Global settings & variables
 */
struct Global 
{
    int     platform;
    int     device;
    bool    useSvm;
    int     verbose;

    bool    isAlgGpuBitonicLDS;
    bool    isAlgGpuBitonicNoLDS;
    bool    isAlgGpuBitonicFull;
    
    bool    isAlgPartialMerge;
    bool    isAlgFullMerge;
    bool    isAlgStlSort;
    bool    isAlgParStlSort;
    bool    isAlgStlQSort;
    bool    isAlgTimSort;

    size_t  globalSize;
    size_t  chunkSize;
    size_t  localSize;
    
    cl_device_id        clDevice;
    cl_context          clContext;
    cl_command_queue    clQueue;

    Profiler profiler;

    Global();
};

extern struct Global GLOBAL;

#endif
