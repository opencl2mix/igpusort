##############################################
   SCOPE
##############################################

Analyze CPU-iGPU performance in sort
1. GPU does partial sort (various configurations chunk-cache-workitem)
2. CPU does full sort on resulting data from 1. (various sorting algorithms)

For questions:
grigore.lupescu@gmail.com

##############################################
   GET & BUILD PROJECT
##############################################

~/Desktop/RESEARCH
$ git clone git@bitbucket.org:opencl2mix/igpusort.git                                                                                                                       1 ↵
Cloning into 'igpusort'...
remote: Counting objects: 13, done.
remote: Compressing objects: 100% (13/13), done.
remote: Total 13 (delta 3), reused 0 (delta 0)
Receiving objects: 100% (13/13), 15.19 KiB | 0 bytes/s, done.
Resolving deltas: 100% (3/3), done.

~/Desktop/RESEARCH
$ cd igpusort
~/Desktop/RESEARCH/igpusort  ‹master›
$ ls
CMakeLists.txt benchmark.sh   helper.cpp     helper.hpp     igpuSort.cpp   sort.cl        timsort.hpp
~/Desktop/RESEARCH/igpusort  ‹master›
$ mkdir build
~/Desktop/RESEARCH/igpusort  ‹master›
$ cd build
~/Desktop/RESEARCH/igpusort/build  ‹master›
$ cmake ..
-- The C compiler identification is AppleClang 8.1.0.8020042
-- The CXX compiler identification is AppleClang 8.1.0.8020042
-- Check for working C compiler: /Library/Developer/CommandLineTools/usr/bin/cc
-- Check for working C compiler: /Library/Developer/CommandLineTools/usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /Library/Developer/CommandLineTools/usr/bin/c++
-- Check for working CXX compiler: /Library/Developer/CommandLineTools/usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Looking for CL_VERSION_2_0
-- Looking for CL_VERSION_2_0 - not found
-- Looking for CL_VERSION_1_2
-- Looking for CL_VERSION_1_2 - not found
-- Looking for CL_VERSION_1_1
-- Looking for CL_VERSION_1_1 - not found
-- Looking for CL_VERSION_1_0
-- Looking for CL_VERSION_1_0 - not found
-- Found OpenCL: /System/Library/Frameworks/OpenCL.framework
-- Configuring done
-- Generating done
-- Build files have been written to: /Users/grigore.lupescu/Desktop/RESEARCH/igpusort/build
~/Desktop/RESEARCH/igpusort/build  ‹master*›
$ cp ../sort.cl .                                                                                                        
~/Desktop/RESEARCH/igpusort/build  ‹master*›
$ make                                                                                                                                                                    127 ↵
Scanning dependencies of target igpuSort
[ 33%] Building CXX object CMakeFiles/igpuSort.dir/igpuSort.cpp.o
[ 66%] Building CXX object CMakeFiles/igpuSort.dir/helper.cpp.o
[100%] Linking CXX executable igpuSort
[100%] Built target igpuSort

##############################################
   RUNNING PROJECT
##############################################

~/Desktop/RESEARCH/igpusort/build  ‹master*›
$ ./igpuSort -h
./igpuSort: option requires an argument -- h
Showcase hybrid sort CPU/GPU
Grigore Lupescu, grigore.lupescu@gmail.com
	-s	vector size
	-p	platform id
	-d	device id
	-s	vector size in exp2, 10=>2^10=>1024
	-c	chunk size in exp2, 9=>2^9=>512
	-l	local size in exp2, 7=>2^7=>128
	-g	 GPU alg (pre-sort):
			0 => bitonic-lds
			1 => bitonic-no-lds
			2 => bitonic-full
	-a	 CPU alg (sort):
			0 => partial-mergesort
			1 => full-mergesort
			2 => STL-sort
			3 => STL-qsort
			4 => TIM-sort
			5 => ParSTL-sort
	-h	print help

~/Desktop/RESEARCH/igpusort/build  ‹master*›
$ ./igpuSort -s 24 -c 10 -l 8 -a 1 -g 0 -d 1
VEC.size, 16777216, CHUNK.size, 1024, GPU.cache, 256, CPU-fmerge-sort, 1386, GPU-LDS, 122, TOTAL, 1508

~/Desktop/RESEARCH/igpusort/build  ‹master*›
$ ./igpuSort -vvv -s 24 -c 10 -l 8 -a 1 -g 0 -d 1                                                                                                                           1 ↵
ARGS (svm = 0, platform = 0, device = 1, CONFIG -> Global, Chunk, Local : VEC.size, 16777216, CHUNK.size, 1024, GPU.cache, 256)
 PRE-SORT (GPU): Pre-sort Bitonic LDS
 SORT (CPU): Algorithm Full Merge
Platforms found: 1
Platform 0 Apple OpenCL 1.2 (May 26 2017 12:59:48)
	Devices found 2
	Device 0 Intel(R) Core(TM) i7-4770HQ CPU @ 2.20GHz OpenCL 1.2
	Device 1 Iris Pro OpenCL 1.2  <--- selected OpenCL device
Validate with STL

Sample: -42261 -42052.4 -36168.6 -36437.9 -35210.9 -33320 -44415.6
Not sorted: 8387154

Sample: -43538 -42074.2 -40612.6 -39149.6 -37688.4 -36225.8 -34763.3
Not sorted: 0
CPU-fmerge-sort (ms) details:
549 -> 1943, 1394
CPU-fmerge-sort, total: 1394ms, avg 1394ms

GPU-LDS (ms) details:
422 -> 549, 127
GPU-LDS, total: 127ms, avg 127ms

TOTAL (ms) details:
422 -> 1943, 1521
TOTAL, total: 1521ms, avg 1521ms

----------------
Start validation with STL