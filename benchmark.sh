#!/bin/bash
for vecSize in {20..27}; do
    for cpuAlg in {1..4}; do
        # No partial sort, full CPU sort
        ./igpuSort -a $cpuAlg -s $vecSize

        # Partial GPU sort + CPU sort
        for lchunk in {8..12}; do
            for lds in {6..8}; do       
                for gpuAlg in {0..1}; do
                    ./igpuSort -a $cpuAlg -g $gpuAlg -s $vecSize -c $lchunk -l $lds -d 0 
                done
            done
        done
    done
done
 

