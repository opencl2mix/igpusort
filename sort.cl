/* plain 1D bitonic sort, somewhat inefficient */
__kernel void bitonic_sort(__global float* data, uint start, uint end)
{
   uint gid = get_global_id(0);
   uint thresh = gid ^ start; 
   
   if(thresh > gid) {
      if((gid & end) == 0) {
         if (data[gid] > data[thresh]) {
            float temp = data[gid];
            data[gid] = data[thresh];
            data[thresh] = temp;
         }
      }
      else {
         if (data[gid] < data[thresh]) {
            float temp = data[gid];
            data[gid] = data[thresh];
            data[thresh] = temp;
         }
      }
   }
}

/* no LDS bitonic sort, good for fast VRAM */
__kernel void bitonic_sort_nolds(__global float* data, uint elemPerWorkItem)
{
    /* get local id and local group size */
    __private uint lid = get_local_id(0);
    __private uint lSize = get_local_size(0);
    
    /* each local group works on a chunk, get its offset and determine size */
    __private uint chunkOffset = (get_global_id(0) - get_local_id(0)) * elemPerWorkItem;
    __private uint chunkSize = get_local_size(0) * elemPerWorkItem;
    
    for(int end = 2; end <= chunkSize; end <<= 1) {
        for(int start = end>>1; start > 0; start >>= 1) {
            
            barrier(CLK_LOCAL_MEM_FENCE);
            
            /* have a cid for each unique item in the chunk */
            for(int cid = lid; cid < chunkSize; cid += lSize)
            {
                uint thresh = start ^ cid;
                if(thresh > cid)
                {
                    if((cid & end) == 0) {
                        if (data[cid + chunkOffset] > data[thresh + chunkOffset]) {
                            float temp = data[cid + chunkOffset];
                            data[cid + chunkOffset] = data[thresh + chunkOffset];
                            data[thresh + chunkOffset] = temp;
                        }
                    }
                    else {
                        if (data[cid + chunkOffset] < data[thresh + chunkOffset]) {
                            float temp = data[cid + chunkOffset];
                            data[cid + chunkOffset] = data[thresh + chunkOffset];
                            data[thresh + chunkOffset] = temp;
                        }
                    }
                }
            }
        }
    }
}

/* LDS based bitonic sort, good for slow VRAM/iGPU */
#define LDS_SIZE    (4 * 1024)

__kernel void bitonic_sort_lds(__global float* ptrData, int elemPerWorkItem)
{
    /* local shared data, max 4k to not limit wavefront/warp count */
    __local float ldata[LDS_SIZE];
    
    /* get local id and local group size */
    __private uint lid = get_local_id(0);
    __private uint lSize = get_local_size(0);
    
    /* each local group works on a chunk, get its offset and determine size */
    __private uint chunkOffset = (get_global_id(0) - get_local_id(0)) * elemPerWorkItem;
    __private uint chunkSize = get_local_size(0) * elemPerWorkItem;
    
    /* copy all data chunk to LDS */
    for(int i = lid; i < chunkSize; i += lSize)
        ldata[i] = ptrData[i + chunkOffset];
    
    for(int end = 2; end <= chunkSize; end <<= 1) {
        for(int start = end>>1; start > 0; start >>= 1) {
            
            barrier(CLK_LOCAL_MEM_FENCE);
            
            /* have a cid for each unique item in the chunk */
            for(int cid = lid; cid < chunkSize; cid += lSize)
            {
                uint thresh = start ^ cid;
                if(thresh > cid)
                {
                    if((cid & end) == 0) {
                        if (ldata[cid] > ldata[thresh]) {
                            float temp = ldata[cid];
                            ldata[cid] = ldata[thresh];
                            ldata[thresh] = temp;
                        }
                    }
                    else {
                        if (ldata[cid] < ldata[thresh]) {
                            float temp = ldata[cid];
                            ldata[cid] = ldata[thresh];
                            ldata[thresh] = temp;
                        }
                    }
                }
            }
        }
    }
    
    /* copy all data chunk to LDS */
    for(int i = lid; i < chunkSize; i += lSize)
        ptrData[i + chunkOffset] = ldata[i];
}
